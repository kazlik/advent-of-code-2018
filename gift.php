<?php
require_once __dir__ . '/numbers.php';
/** @var int[] $numbers */

$result = 0;
foreach ($numbers as $number) {
	$result += $number;
}
echo "Result: $result";